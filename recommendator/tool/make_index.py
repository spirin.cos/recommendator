import argparse
import asyncio
import csv
import json

from recommendator.models.product import Product


async def add_recommends(product, product_2, rank):
    recommends = await product.recommends.get_or_create(pk=rank)
    if not recommends.products:
        recommends.products = '{}'

    dict_recommends = json.loads(recommends.products)

    dict_recommends[product_2.pk] = None

    recommends.update(('products', json.dumps(dict_recommends)))


async def parse_data(path: str):
    with open(path, newline='') as csv_file:
        reader = csv.reader(csv_file)
        for row in reader:
            sku_1, sku_2, rank = row

            product_1 = await Product.get_or_create(sku_1)
            product_2 = await Product.get_or_create(sku_2)

            await add_recommends(product_1, product_2, rank)
            await add_recommends(product_2, product_1, rank)


if __name__ == '__main__':
    main_parser = argparse.ArgumentParser(
        description='Command line tool for parse raw recommends data',
    )

    main_parser.add_argument('-p', '--path', dest='path',
                             type=str,
                             required=True,
                             help=f'path to a file with raw recommends')
    args = main_parser.parse_args()

    loop = asyncio.get_event_loop()
    coro = parse_data(args.path)
    loop.run_until_complete(coro)
