import os
import sys
import json

import logging


class Config:
    def __init__(self, path: str = None) -> None:
        logging.debug("class Config: __init__()")

        if os.path.isfile(path):
            with open(path) as f:
                self.__config = json.loads(f.read())
        else:
            logging.error('ERROR: file "%s" not found' % path)
            sys.exit()

    def get_conf(self):
        return self.__config


def get_config() -> dict:
    obj_conf = Config(os.path.join(os.getcwd(), 'configs/config.json'))
    return obj_conf.get_conf()


config = get_config()
