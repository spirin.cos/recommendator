import os
from typing import Any

from recommendator.lib.configs import config


class FileStorage:
    _path: str

    def init(self, path: str):
        if not os.path.exists(path):
            raise Exception(f"{path} folder that doesn't exist")
        if not os.path.isdir(path):
            raise Exception("the specified path is not the path to the folder")

        self._path = os.path.abspath(path)

    def _get_file_path(self, *args):
        return os.path.join(self._path, *args)

    def dir_exists(self, *args):
        ful_path = self._get_file_path(*args)
        if not os.path.exists(ful_path):
            return False
        return True

    async def get_files_in_path(self, *args):
        ful_path = self._get_file_path(*args)
        return os.listdir(path=ful_path)

    def create_dir(self, *args):
        os.makedirs(self._get_file_path(*args))

    def get_data(self, *args) -> str:
        ful_path = self._get_file_path(*args)

        if os.path.exists(ful_path):
            with open(ful_path) as f:
                data = f.read()
            return data

        with open(ful_path, 'w') as _:
            pass
        return ''

    def save_data(self, *args, data: str):
        ful_path = self._get_file_path(*args)
        with open(ful_path, 'w') as f:
            f.write(data)


files_storage = FileStorage()
files_storage.init(config['path_for_storage'])
