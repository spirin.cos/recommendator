from aiohttp import web

from recommendator.handlers.get_recommends import get_recommends_handle
from recommendator.lib.configs import config

app = web.Application()
app.add_routes(
    [
        web.get('/product/{sku}', get_recommends_handle),
    ]
)

if __name__ == '__main__':
    web.run_app(
        app,
        host=config['host'],
        port=config['port']
    )
