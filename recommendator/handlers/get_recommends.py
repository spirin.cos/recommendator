import json

from aiohttp import web
from aiohttp.web import Response, Request
from aiohttp.web_response import json_response

from recommendator.models.product import Product


async def get_recommends_handle(request: Request):
    sku = request.match_info['sku']
    min_rank = request.query.get('min_rank')
    product = await Product.get(sku)
    if not product:
        return json_response(status=404, data={})

    if min_rank:
        try:
            min_rank = float(min_rank)
        except ValueError:
            return json_response(status=400)
    else:
        min_rank = 0.0

    recommends = await product.recommends.get_all()

    rez = {}
    for recommendation in recommends:
        if float(recommendation.pk) >= min_rank:
            rez[recommendation.pk] = list(json.loads(recommendation.products).keys())

    return json_response(status=200, data=rez)

