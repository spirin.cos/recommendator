from typing import Type, List, Tuple

from recommendator.lib.file_storage import files_storage, FileStorage


class BaseModel:
    pk: str

    _is_init: bool = False
    file_storage: FileStorage = files_storage

    _base_path = ()

    def __init__(self, pk: str):
        self.pk = pk

        for field in dir(self):
            if field.startswith('_'):
                continue

            attr = getattr(self, field)
            if isinstance(attr, Relationship):
                relationship: Relationship = attr
                setattr(self, field, relationship.make(self))

            if issubclass(attr.__class__, BaseField):
                model_field: BaseField = attr
                setattr(self, field, model_field.load(self))

    def get_model_path(self):
        return [*self._base_path, self.pk]

    @classmethod
    def get_base_path(cls) -> list:
        return list(cls._base_path)

    @classmethod
    async def _init_model_class(cls):
        if not cls._is_init:
            cls._base_path = (*cls._base_path, cls.__name__,)

            if not cls.file_storage.dir_exists(*cls.get_base_path()):
                cls.file_storage.create_dir(*cls.get_base_path())
            cls._is_init = True

    @classmethod
    async def create(cls, pk: str):
        await cls._init_model_class()

        model_path = cls.get_base_path()
        model_path.append(pk)

        if cls.file_storage.dir_exists(*model_path):
            raise FileExistsError(f'the {cls.__name__} with the pk={pk} already exists')

        cls.file_storage.create_dir(*model_path)
        return cls(pk=pk)

    @classmethod
    async def get_or_create(cls, pk: str):
        model = await cls.get(pk)
        if not model:
            model = await cls.create(pk)
        return model

    @classmethod
    async def get(cls, pk: str):
        await cls._init_model_class()
        model_path = cls.get_base_path()
        model_path.append(pk)
        if cls.file_storage.dir_exists(*model_path):
            return cls(pk=pk)
        return None

    @classmethod
    async def get_all(cls) -> list:
        await cls._init_model_class()
        models = []
        for file_name in await cls.file_storage.get_files_in_path(*cls.get_base_path()):
            if file_name.startswith('_') or file_name.startswith('.'):
                continue
            models.append(cls(file_name))
        return models

    def update(self, data: Tuple[str, str]):
        name, data = data
        self.file_storage.save_data(*self.get_model_path(), name, data=data)


class BaseField:
    field_class: str

    name: str

    data: str

    def __init__(self, name: str, field_class: any):
        self.field_class = field_class
        self.name = name

    def load(self, model: BaseModel):
        return model.file_storage.get_data(*model.get_model_path(), self.name)


class Relationship:
    related_model_class: Type[BaseModel]

    def __init__(self, related_model_class):
        self.related_model_class = related_model_class

    def make(self, model: BaseModel) -> Type[BaseModel]:
        model_copy = type(f'{self.related_model_class.__name__}',
                          self.related_model_class.__bases__, dict(self.related_model_class.__dict__))
        model_copy._base_path = (*model.get_model_path(), *self.related_model_class.get_base_path())

        return model_copy
